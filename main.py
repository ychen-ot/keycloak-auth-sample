#
# references: 
#    - https://stackoverflow.com/questions/64497615/how-to-add-a-custom-decorator-to-a-fastapi-route
#    - https://github.com/tiangolo/fastapi/issues/2662 - How to write a decorator for path operation?
#    - https://fastapi.tiangolo.com/tutorial/middleware/
#    - https://github.com/tiangolo/fastapi/issues/1428

# Other useful middlewares:
#    - CORS: https://fastapi.tiangolo.com/tutorial/cors/


# -----------------------
# 2 Methods to intercept the request calls. 
#  - decorator
#    - Limitation on getting Request objects.
#    - Request object needs to be part of the wrapper, but also need to be part of the function wrapped.
#    - it is possible to edit the function signature.. to avoid the requirement of the Request
#      (but this also means user would not be able to access request)
#      See: https://stackoverflow.com/questions/64497615/how-to-add-a-custom-decorator-to-a-fastapi-route
#  - middleware.
#    - maybe more useful for things like headers 
#    - This gets run even for the `/docs` and `/redoc` endpoints
#    - or force all traffic to run authorization?
#    - Use middleware to do authorization and to inject role data?

# Usecase two: https://www.keycloak.org/docs/latest/securing_apps/#openid-connect-2


from typing import Optional, Callable
import time

from fastapi import APIRouter, FastAPI, Request, Response, HTTPException, Depends
from fastapi.security import HTTPBearer
from fastapi.routing import APIRoute
# from fastapi.responses import RedirectResponse
from pydantic import BaseModel

from utils.keycloak_utils import KeycloakClient
from jose.exceptions import JWTClaimsError

import inspect


# Decorators:
import functools

def role_permissions(func):
    @functools.wraps(func)                          # ensure function info stays the same.
    def wrapper(request: Request, *args, **kwargs):
        # Do something before
        print("------   role_permission decorator ------")
        print(f"args: {args}")
        print(f"kwargs: {kwargs}")        # intercepts the query parameters. Is there a way to get a request header?
        print(inspect.signature(func))
        value = func(*args, request=request, **kwargs)        # passing in arguments, capture return object
        # Do something after
        return value                         # return the return object
    return wrapper

def role_permissions2(func):
    # if name is the same - will likely get conflict
    # ValueError: duplicate parameter name: 'request'
    def wrapper(_local_request: Request, *args, **kwargs):
        # Do something before
        print("----- role_permission 2 decorator  -------")
        print(f"args: {args}")
        print(f"kwargs: {kwargs}")        # intercepts the query parameters. Is there a way to get a request header?
        print(inspect.signature(func))
        value = func(*args, **kwargs)        # passing in arguments, capture return object
        # Do something after
        return value                         # return the return object

    # Fix signature of wrapper
    # otherwise: validation failure: 
    # {"detail":[{"loc":["query","args"],"msg":"field required","type":"value_error.missing"},
    #      {"loc":["query","kwargs"],"msg":"field required","type":"value_error.missing"}]}%
    import inspect
    wrapper.__signature__ = inspect.Signature(
        parameters = [
            # Use all parameters from func
            *inspect.signature(func).parameters.values(),

            # Skip *args and **kwargs from wrapper parameters:
            *filter(
                lambda p: p.kind not in (inspect.Parameter.VAR_POSITIONAL, inspect.Parameter.VAR_KEYWORD),
                inspect.signature(wrapper).parameters.values()
            )
        ],
        return_annotation = inspect.signature(func).return_annotation,
    )

    return wrapper

  
# --------------------------------

app = FastAPI()

token_auth_scheme = HTTPBearer()

client = KeycloakClient(
            server_url="http://localhost:8081/auth/realm",
            realm_name="education",
            client_id="jakarta-school",
            client_secret_key="QLjHRKca9ElcM1JfJuolS02NxYSWQ0CE"
        )

# Wrapper to decode_and_verify token to throw consistent exceptions.
def _decode_and_verify_token(token):
    try:
        data = client.decode_and_verify_token(token)
    except JWTClaimsError as jwtce:
        raise HTTPException(status_code=401, detail=f"{type(jwtce)} msg: {jwtce}")
    except Exception as e:
        raise HTTPException(status_code=500, detail=f"{type(e)} msg: {e}")
    return data


# Internally - AuthRoute should just verify that user session is available. 
# We will want an UI route... 
class AuthAPIRoute(APIRoute):
    def get_route_handler(self) -> Callable:
        original_route_handler = super().get_route_handler()
        async def custom_route_handler(request: Request) -> Response: 
            user_data = self._verify_login(request)
            if not user_data:
                raise HTTPException(status_code=401, detail="User not found or is not authorized")
            
            print(f"Header: {request.headers}")
            print(f"Request called_url: {request.url}")
            print(f"Request From: {request.url.scheme}://{request.url.netloc}{request.url.path}")
            response = await original_route_handler(request)
            return response
            
        return custom_route_handler
    
    # we can modify the user in the request or should it set in session data?
    def _verify_login(self, request: Request):
        auth_token = request.headers.get("Authorization", None)
        if not auth_token: 
            return None
        else: 
            parts = auth_token.split(" ")
            if len(parts) != 2:
                raise HTTPException(status_code=500, detail="Authorization method not supported.")

            method = parts[0]
            token = parts[1]
            if method.lower() == "bearer":
                data = _decode_and_verify_token(token)
            else:
                raise HTTPException(status_code=500, detail="Authorization method not supported.")

        return data  # data is returned to user_data (in get_router_handler)

# Sample middleware in which you can add header post response 
class TimedRoute(APIRoute):
    def get_route_handler(self) -> Callable:
        original_route_handler = super().get_route_handler()

        async def custom_route_handler(request: Request) -> Response:
            before = time.time()
            response: Response = await original_route_handler(request)
            duration = time.time() - before
            response.headers["X-Response-Time"] = str(duration)
            print(f"route duration: {duration}")
            print(f"route response: {response}")
            print(f"route response headers: {response.headers}")
            return response

        return custom_route_handler


# ---------------------

auth_router = APIRouter(route_class=AuthAPIRoute)
router = APIRouter(route_class=TimedRoute)


@app.middleware("http")
async def add_process_time_header(request: Request, call_next):
    print(" ---------> app.middleware - adding process time in header ------->")
    start_time = time.time()

    # This is where the next call is done...
    response = await call_next(request)

    # continue after call_next executes
    process_time = time.time() - start_time
    response.headers["X-Process-Time"] = str(process_time)
    return response


@app.get("/")
def read_root(request: Request):
    return {"Hello": "World"}

# # Normally, login is where you would route to an external service. 
# # or another service that provides an UID/PASSWD input. 
# # A token would be retrieved here.
# @app.get("/login")
# def login():
#     return {"Login": "No op. Redirect not implemented"}

@app.get("/get_token")
def get_user_token_from_keycloak(user: str, password: str, request: Request):
    return client.get_user_token(uid=user, passwd=password)


@app.get("/items/{item_id}")
@role_permissions
def get_item(item_id: str, request: Request):
    return { "msg": ( "Request header consumed by decorator. NO signature update. "
                      "Function still requires a request object" ),
            "client_host": request.client.host,
            "item_id": item_id }

@app.get("/boxes/{box_id}")
@role_permissions2
def get_boxes(box_id: int):
    return { "msg": ( "Request header consumed by decorator, and signature updated. "
                      "Function did not require a request object" ),
            "box_id": box_id }

# This causes problem for role_permissions2 decorator. 
# @app.get("/many_things/{item_id}")
# @role_permissions2
# def get_many_things(item_id: str, request: Request):
#     client_host = request.client.host
#     return {"client_host": client_host, "item_id": item_id}

@router.get("/timed")
async def timed():
    return {"message": "A test to the time middleware"}

@app.get("/auth/decode_verify_token")
def decode_token(token: str = Depends(token_auth_scheme)):
    # this also verifies the token.
    data = _decode_and_verify_token(token.credentials)
    return data

# endpoints for testing keycloak resources
# still haven't figure out how to hook this in yet. 
@app.get("/auth/general/{asset_id}")
def get_general(asset_id: int, token: str = Depends(token_auth_scheme)):
    return {"asset_id": asset_id,
            "content": "Resource availale for all general users"}

@app.get("/auth/teacher/{asset_id}")
def get_teacher(asset_id: int, token: str = Depends(token_auth_scheme)):
    return {"asset_id": asset_id,
            "content": "Resource availale for all teachers"}

@app.get("/auth/student/{asset_id}")
def get_teacher(asset_id: int, token: str = Depends(token_auth_scheme)):
    return {"asset_id": asset_id,
            "content": "Resource availale for all student"}



# This can be accessed via curl: curl -X GET -H"Authorization: Bearer $TOKEN" "http://localhost:8000/auth/asset/1"
# payload data is consumed.  In addition, not sure how the data is passed (maybe this needs to be passed in the header)
@auth_router.get("/auth/asset/{asset_id}")
def get_asset(asset_id: str, request: Request):
    return { "msg": ( "Auth router function passed. Request was intercepted and token verified." ),
            "asset_id": asset_id }

app.include_router(auth_router)
