from keycloak_utils import KeycloakClient
import keycloak

client = KeycloakClient(
            server_url="http://localhost:8081/auth/realm",
            realm_name="education",
            client_id="jakarta-school",
            client_secret_key="QLjHRKca9ElcM1JfJuolS02NxYSWQ0CE"
        )

user="ying_ed"
passwd="admin"

# Normal flow
print("------- Testing normal flow -----------")
token = client.get_user_token(user, passwd)
print("Token Decoded: {}".format(client.decode_and_verify_token(token)))
print("User Info: {}".format(client.get_user_info(token)))
client.logout(token)

# Accessing token after logout
print("------- Testing accessing user info after logout ----------")
try:
    print(client.get_user_info(token))
except keycloak.exceptions.KeycloakAuthenticationError as e: 
    print(f"Authentication Error. Msg: {e}")


print("------- Testing bad user password  -----------")
try:
    token = client.get_user_token(user, "badpassword")
except keycloak.exceptions.KeycloakAuthenticationError as e:
    print(f"Authentication Error. Msg: {e}")

print("")