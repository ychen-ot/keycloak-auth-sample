# keycloak auth sample

Sample application using FastAPI to interface with keycloak to retrieve authorization information


## Getting started

In order to run this application and test out permissions, you'll need to configure a keycloak client instance. 
Once this is configured, the application can be launched and tested.

The python package that interfaces with keycloak is `python-keycloak`

#### Keycloak configuration

Steps:
1. Download a docker image of keycloak, and launch it, with admin user id and password parameters.
```
# 16.1.1 container map local port 8081 to keycloak. NOTE: local port 8081 is used to map to container 8080 port.
docker run -p 8081:8080 -e KEYCLOAK_USER=admin -e KEYCLOAK_PASSWORD=admin jboss/keycloak

# 17.0.0 container (if needed)
docker run -p 8080:8080 -e KEYCLOAK_ADMIN=admin -e KEYCLOAK_ADMIN_PASSWORD=admin quay.io/keycloak/keycloak:17.0.0 start-dev

```

2. Log into Keycloak.  Once you login, you'll immediately be provided with `master` realm. You'll want to create your own realm.  `master` realm is the realm in which you can manage other realms from keycloak.

3. You will need to create a client and user:
   - Import of the client is available via `sample/jakarta-school.json` file. 
      - If import client, create an user.  Edit role mapping of the user to:
         - realm role: `teacher`
         - client roles: jarkata-school : `create-student-grade`, `view-student-grade`, `view-student-profile`
         - NOTE: If the user you created does not have any roles under the client, then likely you will not be able to access that client.
   - Or, clients and user creatgionCreate client steps and user are available here: 
      - [API login and JWT token generation using Keycloak](https://developers.redhat.com/blog/2020/01/29/api-login-and-jwt-token-generation-using-keycloak)
      - [Authentication and authorization using the Keycloak REST API](https://developers.redhat.com/blog/2020/11/24/authentication-and-authorization-using-the-keycloak-rest-api#)
      - NOTE: You need to click `Save` after you set `Access Type: Confidential`.  Only when this is done, the `client_id` and `client_secret` is available. 

4. NOTE: You can use `docker stop` to stop the container when you are not using, or `docker start` to start a container that has been stopped. If you use `docker rm` to remove the container after it has been stopped, all your configurations to keycloak will be lost 

### Application configuration

Steps:
1. Create the conda environment, then conda activate
```
conda env create -f environment.yml
conda activate fastapi_keycloak
```

2. Update the `main.py` to provide your own realm / client_id and client_secret. 

3. Run
```
uvicorn main:app --reload
```

4. Navigate to 
```
http://127.0.0.1:8000/docs
```

5. You can first retrieve a user token from the `/get_token` endpoint, then use the LOCK icon, to provide to the application the Bearer token needed for some of the routes that are locked.  NOTE: if your user does not have access to the `jarkata-school` client (ie no roles for this user permission), the token will not be valid. 

6. You can use `curl -X GET -H "Authorization: Bearer $ACCESS_TOKEN" "http://localhost:8000/<path>"` to test passing token to the endpoint.

7. NOTE: Most of the `main.py` code is built to test how FastAPI can work with middleware / decorators / and Depends, so that we can get a sense of how the application `/docs` can look.  

 
